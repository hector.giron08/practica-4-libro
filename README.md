# Practica 4

En esta cuarta práctica se muestra los ejercicios del libro escogido para
esta materia.

![517XL4pO6jL._SX376_BO1_204_203_200_](/uploads/c01eefe3292e5de6a76d9a8a05ed23cc/517XL4pO6jL._SX376_BO1_204_203_200_.jpg)

# Requisitos Previos

# ¿Que es Python?
El nombre Python proviene del surrealista grupo de comedia británica Monty 
Pitón,no de la serpiente. A los programadores de Python se les llama 
cariñosamente Pythonistas, y tanto las referencias a Monty Python como a las 
serpientes suelen ser de pimienta.
Python es un lenguaje de programación de alto nivel, con aplicaciones en 
numerosas áreas, que incluyen programación web, scripts, computación científica 
e inteligencia artificial. 
Python es procesado en tiempo de ejecución por el intérprete . No es necesario 
compilar su programa antes de ejecutarlo.
Es muy popular y utilizado por organizaciones como Google, la NASA, la CIA y 
Disney.

# Instalación 
El intérprete de Python se puede descargar gratuitamente desde http://python.org, 
y hay versiones para Linux, OS X y Windows.
Tenga en cuenta la >>> en el código de arriba. Son el símbolo de la consola de 
Python. Python es un lenguaje interpretado, lo que significa que cada línea se 
ejecuta a medida que se ingresa. Python también incluye IDLE , el entorno de 
desarrollo integrado, que incluye herramientas para escribir y depurar programas
completos.
Encontrará instaladores de Python para equipos de 64 y 32 bits para cada sistema
operativo en la página de descargas, así que primero averigüe qué instalador 
necesita. Si compró su computadora en 2007 o después, lo más probable es que sea
un sistema de 64 bits. De lo contrario, tiene una versión de 32 bits, pero aquí 
está cómo averiguarlo con seguridad:

**Windows**
* Seleccione Start4Control Panel4System y compruebe si System Type 
dice 64 bits o 32 bits.
* Despues descargue el instalador de Python (el nombre de archivo 
terminará con.msi) y haga doble clic en él. Siga las instrucciones del 
instalador en la pantalla para instalar Python, como se indica aquí:
    1. Seleccione Instalar para todos los usuarios y, a continuación, haga 
    clic en Siguiente.
    2. Instale en la carpeta C:\Python34 haciendo clic en Siguiente.
    3. Haga clic de nuevo en Siguiente para omitir la sección Personalizar 
    Python.

**IDLE:** Si usted tien Windows XP, haga click en el botón Inicio y seleccione 
Programas y luego IDLE. Si tiene Windows 7 o posterior haga click en el icono
de inicio e introduza IDLE.

**OS X**
* Vaya al menú de Apple, seleccione Acerca de este Mac4Más 
información4System Report4Hardware, y luego mire el nombre del procesador campo.
Si dice Intel Core Solo o Intel Core Duo, usted tiene una maquina con disco duro
de 32 bits. Si dice algo más (incluyendo Intel Core 2 Duo), usted tienen una 
máquina de 64 bits.
* Despues descargue el archivo.dmg adecuado para su versión de OS X y haga 
doble clic en él. Siga las instrucciones que el instalador muestra en la pantalla
para instalar Python, como se indica aquí:
    1. Cuando el paquete DMG se abra en una nueva ventana, haga doble clic en el
    botón Archivo Python.mpkg. Es posible que tenga que introducir la contraseña
    de administrador.
    2. Haga clic en Continuar en la sección de Bienvenida y haga clic en Aceptar
    para aceptar. la licencia.
    3. Seleccione HD Macintosh (o el nombre que tenga su disco duro) y haga clic
    en Instalar.

**IDLE:** Abra la ventana Finder, haga clic en Aplicaciones, haga clic en Python 
3.4 y, a continuación, en el icono IDLE. 


**Ubuntu Linux**
* Abra un Terminal y ejecute el comando uname -m. Una respuesta
de i686 significa 32 bits, y x86_64 significa 64 bits.
* Despues instale Python siguiendo estos pasos:
    1. Introduzca sudo apt-get install python3.
    2. Introduzca sudo apt-get install idle3.
    3. Introduzca sudo apt-get install python3-pip.
    
**IDLE:** Seleccione Applications4Accessories4Terminal y luego ingrese idle3. 
(También puede hacer clic en Applications en la parte superior de la pantalla, 
seleccionar Programming, y luego en IDLE.). 

# Introduccion
**Automate the Boring Stuff with Python**

El software está en el centro de muchas de las herramientas que utilizamos hoy 
en día: Casi todo el mundo utiliza las redes sociales para comunicarse, muchas 
personas tienen computadoras conectadas a Internet en sus teléfonos, y la 
mayoría de los trabajos de oficina implican interactuar con una computadora para
realizar el trabajo. Como resultado, la demanda de personas que pueden codificar
se ha disparado. Incontables libros, tutoriales web interactivos y campamentos 
de entrenamiento para desarrolladores prometen convertir a los principiantes 
ambiciosos en ingenieros de software con salarios de seis cifras. Este libro no 
es para esa gente. Es para todos los demás. Por sí solo, este libro no lo 
convertirá en un desarrollador de software profesional, más que unas pocas 
lecciones de guitarra lo convertirán en una estrella del rock. Pero si usted es 
un oficinista, administrador, académico o cualquier otra persona que utilice una
computadora para trabajar o divertirse, aprenderá los conceptos básicos de
programación para que pueda automatizar tareas simples.

# Part 1: Python Programming Basics

La primera parte de este libro cubre los conceptos básicos de programación de 
Python. He aquí un breve resumen de lo que encontrará en cada 
capítulo: 

- Capitulo 1: Fundamentos de Pyhton
    - [Practica 1](hector.giron08/practica-4-libro#3)
- Capitulo 2: Control de Flujo
    - [Practica 2](hector.giron08/practica-4-libro#4)
- Capitulo 3: Funciones
    - [Practica 3](hector.giron08/practica-4-libro#5)
- Capitulo 4: Listas
    - [Practica 4](hector.giron08/practica-4-libro#6)
- Capitulo 5: Diccionarios y datos de estructuración
    - [Practica 5](hector.giron08/practica-4-libro#7)
- Capitulo 6: Manipulación de cadenas
    - [Practica 6](hector.giron08/practica-4-libro#8)

# Part 2: Automating tasks

la segunda parte cubre varias tareas que usted puede automatizar con 
su computadora. He aquí un breve resumen de lo que encontrará en cada 
capítulo:

- Capitulo 7: Emparejamiento de Patrones con Expresiones Regulares
    - [Practica 7](hector.giron08/practica-4-libro#9)
- Capitulo 8: Lectura y escritura de archivos 
    - [Practica 8](hector.giron08/practica-4-libro#10)
- Capitulo 9: Organización de archivos 
    - [Practica 9](hector.giron08/practica-4-libro#11)
- Capitulo 10: Depuración 
    - [Practica 10](hector.giron08/practica-4-libro#12)

# Archivos
- [Primera Parte](https://gitlab.com/hector.giron08/practica-4-libro/tree/master/Archivos/Primera%20Parte)

- [Segunda Parte](https://gitlab.com/hector.giron08/practica-4-libro/tree/master/Archivos/Segunda%20Parte)

# Licencia

[Licencia.](5d95f9f551276ba33f490e5f30d91700da44b14c)

# Autor

Héctor Octavio Girón Bobadilla