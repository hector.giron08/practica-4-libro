import re

string = input("Introduzca una cadena para despojar: ")
bad_chars = input("Introduce los caracteres que quieres que sean segmentados: ")

def regex_strip(string, bad_chars):
   
    strip_regex = re.compile('[%s]' % bad_chars)
    return strip_regex.sub('', string)
    
try:
    print(regex_strip(string, bad_chars))
except TypeError:
    print(regex_strip(string, ' '))
