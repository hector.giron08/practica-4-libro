
import re

# user input
print("\nThis program searches for lines with your string in them\n")
search_str = input("Please write the string you are searching for: \n")
print("")
# file input
file = open("C:/Users/GIRON/Documents/1 Maestria/3.-Materias/3.Herramientas basicas para el desarrollo de software-M Agustin/hola.txt")
content = file.read()
file.close()

# create regex
regex = re.compile(r".*(%s).*" % search_str)

# print out the lines with match
if regex.search(content) is None:
    print("No matches was found.")
else:
    print(regex.findall(content))
